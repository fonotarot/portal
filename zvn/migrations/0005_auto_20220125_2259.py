# Generated by Django 3.2.11 on 2022-01-26 01:59

import django.db.models.deletion
import jsonfield.fields
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("zvn", "0004_auto_20220112_0416"),
    ]

    operations = [
        migrations.AddField(
            model_name="formulario",
            name="brokers",
            field=jsonfield.fields.JSONField(default=dict),
        ),
        migrations.AddField(
            model_name="producto",
            name="formulario",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to="zvn.formulario",
            ),
        ),
    ]
