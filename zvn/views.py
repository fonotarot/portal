import json
import logging

from django.apps import apps
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ImproperlyConfigured
from django.http import HttpResponseNotFound, HttpResponseServerError, JsonResponse
from django.shortcuts import redirect, render
from django.template.loader import render_to_string
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.list import ListView
from thankyou import give_thanks

from core.tasks import send_email
from zvn import models, stuff
from zvn.tasks import procesa_pago, procesa_pago_firenze

logger = logging.getLogger("django")


class Telefonos(LoginRequiredMixin, ListView):
    model = models.Telefono
    template_name = "zvn/lista_telefonos.html"
    context_object_name = "telefonos"

    def get_queryset(self):
        return models.Telefono.objects.filter(user=self.request.user, activo=1)


@login_required
def elimina_telefono(request, telefono_num):
    telefono = models.Telefono.objects.filter(
        numero=telefono_num, user=request.user
    ).first()
    if not telefono:
        return HttpResponseNotFound("Not Found")
    telefono.activo = 0
    telefono.save()
    return redirect(reverse("telefonos"))


@login_required
def agrega_telefono(request, telefono_num):
    telefono = models.Telefono.objects.filter(
        numero=telefono_num, user=request.user
    ).first()
    if telefono:
        return HttpResponseServerError("Numero ya existe")
    nuevo_telefono = models.Telefono(user=request.user, numero=telefono_num, activo=1)
    nuevo_telefono.save()
    return redirect(reverse("telefonos"))


@login_required
def selecciona_formulario(request):
    ids_servicio = []
    servicios = models.Cliente.objects.filter(user_id=request.user.id).all()
    if servicios:
        for s in servicios:
            ids_servicio.append(s.id)
    if not ids_servicio:
        formulario = models.Formulario.objects.filter(activo=1, servicio__activo=1)
    else:
        formulario = models.Formulario.objects.filter(
            activo=1, servicio__activo=1, servicio__in=ids_servicio
        )
    context = {
        "formularios": formulario,
    }
    return render(request, "zvn/seleccion_formulario.html", context=context)


@csrf_exempt
def notificacion_merchants(request, transaccion):
    compra = models.Compra.objects.filter(transaccion=transaccion).first()
    if not compra:
        logger.warn(f"Compra {transaccion} no se encuentra")
        return JsonResponse({"status": "NOT_FOUND", "msg": give_thanks()})

    if compra.estado not in ["Pendiente", "Expirado"]:
        logger.warn(f"Compra {transaccion} con estado {compra.estado}")
        return JsonResponse({"status": "ALREADY_PROCESED", "msg": give_thanks()})

    pago = json.loads(request.body)
    # 🤷🤷🤷🤷🤷🤷🤷🤷🤷🤷
    pago = json.loads(pago)

    if ("status" in pago) and (pago["status"] == "Paid"):
        if compra.formulario.servicio.slug in ["fonotarot-latam"]:
            logger.info(
                f"procesa_pago_firenze(kwargs={{transaccion: {compra.transaccion}}}, countdown=5)"
            )
            procesa_pago_firenze.apply_async(
                kwargs={"transaccion": compra.transaccion}, countdown=5
            )

        else:
            logger.info(
                f"procesa_pago.apply_async(kwargs={{transaccion: {compra.transaccion}}}, countdown=5)"
            )
            procesa_pago.apply_async(
                kwargs={"transaccion": compra.transaccion}, countdown=5
            )
        # Aqui estaba el envio del pin, actualizar funcion
    return JsonResponse({"status": "OK", "msg": give_thanks()})
