# Changelog

Registro de cambios notables de ZVN Portal  

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

- Translations: es, en

## [2024.4.1] - 2024-04-10

### Removed

- TODO: matomo tag, Google UA

### Added

- formulario nextgen con tabler, TODO: incorporar a LATAM
- Tienda
- Soporte markdown & mdeditor

### BUG

- formulario.fid no acepta uuid4

## [2024.4.0] - 2024-04-01

### Changed

- Semvar scheme a AÑO.MES.BUILD

## [0.2.3] - 2023-04-08

### Added

- Fonotarot Latam injecta a Firenze
- stuff.crea_cliente_portal lee desde Firenze
- stuff.crea_cliente_firenze
- tasks.procesa_pago_firenze

### Changed

- Firenze APi has its own file

## [0.2.2] - 2023-04-08

### Added

- Checkout para Wordpress
- JS Track: gtag, matomo
- Merchants
- Favicon

### Changed

- Python 3.11+

### Removed

- csvexport
- Reestablece archivos formulario

## [0.2.1] - 2022-10-26

### Changed

- Problema N+1 (#12)
- User Panel (#13)
- Correo Notificacion Compra (#15)
- Bug Correo notificacion compra

## [0.2.0] - 2022-10-26

### Added

- Soporte Adnetworks
- Soporte postback
- Procesa pago como accion en admin
- Paginacion admin

### Changed

- Template por defecto si no se encuentra el definido
- static en 400.html y 500.html (considerar actualizarlos)

## [0.1.1] - 2022-09-25

### Changed

- tarot.LandingView reemplaza views.FormularioCompra
- Nuevo Html para el landing

### Added

- Signals: Compra:pre_save, post_save - Formulario:post_save

## [0.1.0] - 2022-08-22

### Added

- Celery

## [0.0.1] - 2021-07-08

### Added

- Modulo ZVN (#1)
- Casos de Pruebas Formulario (#2)

| Telefono  | Servicio     | Caso          |  
| --------- | ------------ | ------------- |
| 957845632 | fonotarot-cl | usuario registrado |
| 854751235 | fonotarot-cl | usuario registrado mismo 957845632 |
| 568452145 | fonotarot-cl | usuario registrado sin clientid |
