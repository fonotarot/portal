import json
import logging
import secrets
import string
from typing import Any
from urllib.parse import parse_qs

from django import forms
from django.db.models import F
from django.http import (
    HttpResponse,
    HttpResponseNotFound,
    HttpResponseRedirect,
    HttpResponseServerError,
    JsonResponse,
    QueryDict,
)
from django.shortcuts import redirect, render
from django.template import TemplateDoesNotExist
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from validate_email import validate_email

from zvn.models import Compra, Formulario, Producto, Promocion, Tienda
from zvn.stuff import (
    busca_cliente_portal,
    busca_formulario,
    busca_tienda,
    crea_pack,
    get_host_for_real,
    lee_pack,
    lista_productos,
)

logger = logging.getLogger("django")


class LandingForm(forms.Form):
    pais = forms.CharField(max_length=2, required=True)
    username = forms.CharField(max_length=255, required=True)
    producto = forms.IntegerField(required=True)
    broker = forms.CharField(required=True)
    parametros_extra = forms.CharField(max_length=1000, required=False)
    telefono_e164 = forms.CharField(max_length=255, required=False)


class CheckoutForm(forms.Form):
    nombre = forms.CharField(required=True)
    correo = forms.EmailField(min_length=5, required=True)
    telefono = forms.CharField(max_length=16, required=True)
    extra = forms.CharField(max_length=1000, required=False)
    pack = forms.CharField(max_length=1000, required=True)


class Landing(View):
    hostname: str = None
    formulario: Formulario = None
    tienda: Tienda = None
    extra_params: str = ""

    def setup(self, request, *args, **kwargs):
        super(Landing, self).setup(request, *args, **kwargs)
        self.hostname = get_host_for_real(self)
        self.formulario = busca_formulario(self.hostname)
        # self.tienda = busca_tienda(self.hostname)

    def get(self, request, *args, **kwargs) -> HttpResponse | HttpResponseRedirect:
        if self.tienda:
            return redirect(reverse("tiendas", args=[self.tienda.slug]), permanent=False)
        if not self.formulario:
            return redirect(reverse("login"), permanent=False)

        self.obtiene_parametros(request)
        productos = lista_productos(self.formulario)
        datos = {
            "formas_pago": self.formulario.servicio.merchant_brokers,
            "google_ua": self.formulario.google_ua,
            "productos": productos,
            "parametros_extra": self.extra_params,
            "default_country": self.formulario.pais,
        }
        logger.debug(f"Cargando formulario para {self.hostname}")
        try:
            template_render = render(
                request,
                f"zvn/landing/{self.formulario.archivo_landing}",
                datos,
            )
        except TemplateDoesNotExist as e:
            logger.warning(f"No se encontró {self.formulario.archivo_landing}, cargando skel")
            template_render = render(
                request,
                f"zvn/formularios/skel/landing.html",
                datos,
            )
        return template_render

    def post(self, request, *args, **kwargs) -> HttpResponse | HttpResponseRedirect:
        if not self.formulario:
            return redirect(reverse("login"), permanent=False)

        compra = LandingForm(request.POST)
        if not compra.is_valid():
            # logger.error(compra.errors)
            return redirect(reverse("landing"), permanent=False)
        ### No buscar usuario en esta etapa, no es necesario
        producto = Producto.objects.get(id=compra.cleaned_data.get("producto"))
        broker_info = compra.cleaned_data.get("broker").split("::")
        purchase_pack = crea_pack(broker_info[0], producto.id)
        datos = {
            "broker_name": broker_info[1],
            "producto": producto,
            "sign": purchase_pack,
            "parametros_extra": compra.cleaned_data.get("parametros_extra"),
        }
        if validate_email(compra.cleaned_data.get("username")):
            datos["correo"] = compra.cleaned_data.get("username")

        if not validate_email(compra.cleaned_data.get("username")):
            datos["telefono"] = compra.cleaned_data.get("telefono_e164").replace("+", "")

        try:
            template_render = render(
                request,
                f"zvn/landing/{self.formulario.archivo_checkout}",
                datos,
            )
        except TemplateDoesNotExist as e:
            logger.warning(f"No se encontró {self.formulario.archivo_checkout}, cargando skel")
            template_render = render(
                request,
                f"zvn/formularios/skel/checkout.html",
                datos,
            )
        return template_render

    def put(self, request, *args, **kwargs) -> JsonResponse | HttpResponseRedirect:
        if not self.formulario:
            return redirect(reverse("login"), permanent=False)
        data = QueryDict(request.body)
        compra = CheckoutForm(data)
        if not compra.is_valid():
            logger.error(compra.errors)
            return HttpResponseServerError("Form Not Valid, check logs")

        extra_qs = None
        if "extra" in data and (data["extra"] != "None"):
            extra_qs = parse_qs(
                data["extra"],
                keep_blank_values=True,
                encoding="utf-8",
                errors="replace",
            )
            for k, v in extra_qs.items():
                extra_qs[k] = "-".join(v)

        pack = lee_pack(data["pack"])
        logger.info(f"{pack = }")

        compra = Compra(
            formulario=self.formulario,
            broker=pack["b"],
            producto_id=pack["p"],
            correo=data["correo"],
            telefono=data["telefono"],
            nombre=data["nombre"],
            extra=extra_qs,
        )

        try:
            logger.info(f"{compra = }")
            compra.save()
        except Exception as error:
            logger.error(f"{error=}")
            return HttpResponseServerError("No se pudo crear el pago")

        if not compra.url:
            return HttpResponseServerError("No se pudo crear el pago")

        salida = {"url": compra.url, "transaccion": compra.transaccion}
        return JsonResponse(salida)

    def obtiene_parametros(self, request) -> bool:
        if len(request.GET) > 0:
            self.extra_params = request.GET.copy().urlencode()
            return True
        return False


class Promociones(View):
    hostname: str = None
    formulario: Formulario = None

    def setup(self, request, *args, **kwargs):
        super(Promociones, self).setup(request, *args, **kwargs)
        self.hostname = get_host_for_real(self)
        self.formulario = busca_formulario(self.hostname)

    def get(self, request, *args, **kwargs) -> HttpResponse | HttpResponseRedirect:
        if not self.formulario:
            logger.warn(f"{self.formulario=}")
            return redirect(reverse("landing"), permanent=False)
        datos = {}
        parametros = request.GET.copy().dict()
        if "promo_code" not in parametros:
            logger.warn("promo_code not in GET")
            return redirect(reverse("landing"), permanent=False)

        datos["promo_code"] = parametros.get("promo_code")
        datos["GET"] = parametros
        try:
            campana = Promocion.objects.get(slug=parametros.get("promo_code"), activo=True)
            datos["campana"] = campana
        except Promocion.DoesNotExist:
            logger.warn("Promocion.DoesNotExist")
            return redirect(reverse("landing"), permanent=False)
        else:
            campana.prints = F("prints") + 1
            campana.save()
        parametros["promo_id"] = campana.pk
        carta = request.path.split("/")[1]
        cartas = {
            "la.emperatriz": "https://source.unsplash.com/lbY2IVfO9bc/1920x1080",
            "el.mago": "https://source.unsplash.com/1dIh5H5BMvI/1920x1080",
            "la.estrella": "https://source.unsplash.com/XZhILkqs2mk/1920x1080",
            "as.de.oros": "https://source.unsplash.com/y3qrbAgm7q8/1920x1080",
            "la.rueda.de.la.fortuna": "https://source.unsplash.com/fQHiXNWr_xo/1920x1080",
            "tarot": "https://source.unsplash.com/XoBdj1zV-EA/1920x1080",
        }
        fondo = cartas.get("tarot") if carta not in cartas else cartas.get(carta)
        datos["fondo"] = fondo
        if "cust_email" in parametros:
            datos["cust_email"] = parametros.get("cust_email")
        else:
            datos["GET"] = parametros
            datos["default_country"] = self.formulario.pais
            return render(request, "zvn/formularios/necesito_tu_correo.html", datos)

        numeros = string.digits
        telefono_aleatorio = "".join(secrets.choice(numeros) for i in range(8))
        if campana.solo_nuevos:
            if busca_cliente_portal(datos["cust_email"], telefono_aleatorio, campana.formulario.servicio):
                logger.warn(f"{datos['cust_email']} ya está registrado y {campana.solo_nuevos=}")
                return redirect(reverse("landing"), permanent=False)
        compra = Compra(
            formulario=self.formulario,
            broker=campana.broker_slug,
            producto_id=campana.producto.pk,
            correo=datos["cust_email"],
            telefono=f"561{telefono_aleatorio}",
            extra=parametros,
            es_promocion=True,
        )
        try:
            compra.save()
        except Exception as error:
            logger.warn(f"{error=}")
            return HttpResponseServerError("No se pudo crear el pago")
        logger.info(f"{compra=}")
        if not compra.url:
            logger.warn(f"{compra.url=}")
            return HttpResponseServerError("No se pudo crear el pago")

        salida = {"url": compra.url, "transaccion": compra.transaccion}
        logger.info(f"{salida=}")

        campana.clicks = F("clicks") + 1
        campana.save()

        return redirect(salida["url"])


class WP_Checkout(View):
    hostname: str = None
    formulario: Formulario = None

    def setup(self, request, *args, **kwargs):
        super(WP_Checkout, self).setup(request, *args, **kwargs)
        self.hostname = get_host_for_real(self)
        self.formulario = busca_formulario(self.hostname)

    def get(self, request, *args, **kwargs) -> HttpResponse | HttpResponseRedirect:
        if not self.formulario:
            logger.warn(f"{self.formulario=}")
            return redirect("https://fonotarot.com", permanent=False)
        datos = {}
        query_string = request.GET.copy()
        datos["qs"] = query_string.urlencode()
        if "origin" not in self.kwargs:
            logger.warn("origin not set")
            return redirect("https://fonotarot.com", permanent=False)

        try:
            origen = Promocion.objects.get(slug=self.kwargs.get("origin"), activo=True)
            datos["origen"] = origen
        except Promocion.DoesNotExist:
            logger.warn("Promocion.DoesNotExist")
            return redirect("https://fonotarot.com", permanent=False)
        else:
            origen.prints = F("prints") + 1
            origen.save()
        datos["default_country"] = self.formulario.pais
        return render(request, "zvn/formularios/wp-checkout.html", datos)


@method_decorator(csrf_exempt, name="dispatch")
class ProcessPayment(View):
    """Fonotarot ProcessPayment"""

    hostname: str = None
    formulario: Formulario = None

    def setup(self, request, *args, **kwargs):
        """ProcessPayment.setup"""
        super(ProcessPayment, self).setup(request, *args, **kwargs)
        self.hostname = get_host_for_real(self)
        self.formulario = busca_formulario(self.hostname)

    def put(self, request, *args, **kwargs) -> JsonResponse | HttpResponseNotFound:
        """ProcessPayment.put"""
        if not self.formulario:
            logger.warn(f"{self.formulario=}")
            return HttpResponseNotFound()

        if "servicio" not in self.kwargs or "formulario" not in self.kwargs:
            logger.warn(f"{self.kwargs=}")
            return HttpResponseNotFound()

        if self.formulario.fid != self.kwargs.get("formulario"):
            logger.warn(f"{self.formulario.fid=} != {self.kwargs.get('formulario')=}")
            return HttpResponseNotFound()

        try:
            formulario = Formulario.objects.get(fid=self.kwargs.get("formulario"), activo=1)
        except Formulario.DoesNotExist:
            logger.warning(f"{self.kwargs.get('formulario')=} DoesNotExist")
            return HttpResponseNotFound()

        if "Content-Type" in request.headers and "application/json" in request.headers.get("Content-Type"):
            body = json.loads(request.body)
        else:
            body = QueryDict(request.body)
        query_string = request.GET.copy().dict()
        headers = request.headers

        try:
            producto = Producto.objects.get(slug=body.get("product"), activo=1)
        except Producto.DoesNotExist:
            logger.warn(f"{body.get('product')=} no existe en Productos")
            return HttpResponseNotFound()

        logger.info(f"{self.kwargs=}")
        logger.info(f"{self.args=}")
        logger.info(f"{body=}")
        logger.info(f"{query_string=}")
        logger.info(f"{headers=}")

        broker = body.get("broker", None)
        extra = {
            "body": body,
            "qs": query_string,
            "headers": headers.to_wsgi_names(headers),
            "args": self.args,
            "kwargs": {kw: str(arg) for kw, arg in self.kwargs.items()},
        }
        logger.info(f"{extra=}")
        rand_tel = "".join(secrets.choice(string.digits) for i in range(9))
        compra = Compra(
            formulario=self.formulario,
            broker=f"{broker}-fonotarot",
            producto_id=producto.id,
            correo=body.get("correo", None),
            telefono=f"56{rand_tel}",
            nombre=body.get("nombre", None),
            extra=extra,
        )
        try:
            compra.save()
        except Exception as error:
            logger.error(f"{error=}")
            return HttpResponseServerError("No se pudo crear el pago")

        if not compra.url:
            return HttpResponseServerError("No se pudo crear el pago")

        salida = {"url": compra.url, "transaccion": compra.transaccion}
        return JsonResponse(salida)
