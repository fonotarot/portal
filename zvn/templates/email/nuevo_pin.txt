{{asunto}}

====
Información de Cliente
Nombre
    {{ compra.nombre|default_if_none:"Sin nombre" }}
Correo
    {{ compra.correo|default_if_none:"Sin correo" }}
Telefono
    {{ compra.telefono|default_if_none:"Sin teléfono" }}

====
Información de Compra
Número Orden
    {{ compra.transaccion }}
Fecha Creacion
    {{ compra.creado }}
Estado
    {{ compra.estado }}
Minutos
    {% widthratio compra.producto.cambio.segundos 60 1 as minutos %}
    {{ minutos }}
PIN
    {{ compra.cliente.pin }}

====
Numeros Acceso
Argentina: +54 (11) 5218 8159 
Chile: +56 (2) 2230 1520
US: +1 (888) 283 8184


&copy; {% now "Y" %} ZVN Comunicaciones