from baton.autodiscover import admin
from django.urls import path, include
import debug_toolbar

urlpatterns = [
    path("admin/", admin.site.urls),
    path("baton/", include("baton.urls")),
    # path("payments/", include("payments.urls")),
    path("__debug__/", include(debug_toolbar.urls)),
    path("", include("zvn.urls")),
    # path("", include("merchants.urls")),
    path("", include("core.urls")),
    path("mdeditor/", include("mdeditor.urls")),
]
