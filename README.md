Cthulhu
======

![Cthulhu](https://media.giphy.com/media/3o85xzEtQs693ln3qM/giphy.gif)  
  
FullOn CRM for personal and commercial projects  

![Tests](https://github.com/mariofix/cthulhu/workflows/CodeQL/badge.svg)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)  

## Requirements

* Python 3.9 / Poetry
* MariaDB/MySQL
* Django 4.1

## Install

```bash
git init  
git remote add gitlab git@gitlab.com:fonotarot/portal.git    
git checkout -B desarrollo  
git pull github desarrollo  
```

## Run

```bash
poetry update
poetry run python manage.py runserver 8000
```
