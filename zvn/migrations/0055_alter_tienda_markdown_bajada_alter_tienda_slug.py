# Generated by Django 5.0.6 on 2024-06-08 03:25

import mdeditor.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("zvn", "0054_tienda_markdown_bajada_tienda_texto_titulo"),
    ]

    operations = [
        migrations.AlterField(
            model_name="tienda",
            name="markdown_bajada",
            field=mdeditor.fields.MDTextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name="tienda",
            name="slug",
            field=models.SlugField(default=None, max_length=128, null=True),
        ),
    ]
