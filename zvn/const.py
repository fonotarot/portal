CURRENCY = (
    ("CLP", "CLP"),
    ("USD", "USD"),
)

PAISES = (("cl", "Chile"), ("auto", "Autodetectar"))

ACCIONES_POSTBACK = (
    (None, "No Ejecutar Postback"),
    ("GET", "GET"),
    ("POST", "POST"),
    ("PUT", "PUT"),
    ("HEAD", "HEAD"),
    ("OPTIONS", "OPTIONS"),
)


ESTADOS_COMPRA = (
    ("Pendiente", "Pendiente"),
    ("Pagado", "Pagado"),
    ("Expirado", "Expirado"),
    ("Rechazado", "Rechazado"),
)


TIPOS_MENSAJE = (
    ("email", "Correo Electronico"),
    ("sms", "SMS"),
    ("voz", "Voz"),
    ("whatsapp", "Whatsapp"),
    ("telegram", "Telegram"),
    ("email_sync", "Correo Electrónico (Sync)"),
    ("webpush", "Notificacion Browser"),
    ("url", "HTTP/S"),
)
ESTADOS_MENSAJE = (
    ("not_sent", "No Enviado"),
    ("sending", "Enviando"),
    ("sent", "Enviado"),
    ("rejected", "Rechazado"),
)


BROKER_SLUGS = (
    ("paypal-fonotarot", "FonoTarot - PayPal"),
    ("stripe-fonotarot", "FonoTarot - Stripe"),
    ("flow-webpay-fonotarot", "FonoTarot - Flow Chile"),
    ("khipu-fonotarot", "FonoTarot - Khipu"),
    ("flow-webpay-alotarot", "Alotarot - Flow Chile"),
    ("khipu-alotarot", "Alotarot - Khipu"),
)
