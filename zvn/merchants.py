import logging
from dataclasses import dataclass, field
from typing import List

import requests
from django.conf import settings

from .firenze import FirenzeAPI

logger = logging.getLogger("django")


@dataclass
class MerchantAPI:
    api_url: str = settings.MERCHANTS_API_URL
    api_user: str = settings.MERCHANTS_API_USER
    api_pass: str = settings.MERCHANTS_API_PASSWORD
    api_scopes: str = settings.MERCHANTS_API_SCOPES
    api_token: str = None

    def get_token(self):
        if not self.api_token:
            data = {
                "grant_type": "password",
                "scope": self.api_scopes,
                "username": self.api_user,
                "password": self.api_pass,
            }
            request = requests.post(f"{self.api_url}/token", data=data)
            request.raise_for_status()
            datos = request.json()
            self.api_token = datos["access_token"]
        return self.api_token


@dataclass
class Merchant(MerchantAPI):
    def crear_pago(self, payload):
        pago = requests.post(
            f"{self.api_url}/payment/",
            json=payload,
            headers={
                "accept": "application/json",
                "Authorization": f"Bearer {self.get_token()}",
            },
        )
        pago.raise_for_status()
        datos = pago.json()
        if "transaction" not in datos:
            logger.error(f"{datos=}")
            raise Exception("No viene transaction")
        url = requests.get(
            f"{self.api_url}/payment/{datos['transaction']}/start",
            headers={
                "accept": "application/json",
                "Authorization": f"Bearer {self.get_token()}",
            },
        )
        url.raise_for_status()
        return url.json()


@dataclass
class Audiotex(FirenzeAPI):
    def buscar_usuario(self, slug, correo, telefono):
        payload = {"correo": correo, "telefono": telefono}

        busca = requests.get(
            f"{self.api_url}/audiotex/{slug}/client",
            params=payload,
            headers={
                "accept": "application/json",
                "Authorization": f"Bearer {self.get_token()}",
            },
        )
        busca.raise_for_status()
        return busca

    def sumar_saldo(self, slug: str, client_id: int, segundos: int):
        saldo = requests.post(
            f"{self.api_url}/audiotex/{slug}/credit/{client_id}/{segundos}",
            headers={
                "accept": "application/json",
                "Authorization": f"Bearer {self.get_token()}",
            },
        )
        print(f"{saldo.url=}")
        saldo.raise_for_status()

        return saldo

    def crear_cliente(self, slug: str, payload: dict):
        cliente = requests.post(
            f"{self.api_url}/audiotex/{slug}/client/",
            json=payload,
            headers={
                "accept": "application/json",
                "Authorization": f"Bearer {self.get_token()}",
            },
        )
        if cliente.status_code != 200:
            logger.error(cliente.json())
        cliente.raise_for_status()
        return cliente
