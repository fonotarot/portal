import logging
import pprint
import uuid
from typing import Iterable

from django.db import models
from django.db.models.fields import (
    BooleanField,
    CharField,
    DateTimeField,
    FloatField,
    SlugField,
    TextField,
    UUIDField,
)
from django.db.models.fields.json import JSONField
from django.utils import timezone
from django.utils.text import slugify

from mdeditor.fields import MDTextField

from core.models import CoreUser
from zvn.const import (
    ACCIONES_POSTBACK,
    BROKER_SLUGS,
    CURRENCY,
    ESTADOS_COMPRA,
    ESTADOS_MENSAJE,
    PAISES,
    TIPOS_MENSAJE,
)

logger = logging.getLogger("django")


class TimeStampMixin(models.Model):
    creado = DateTimeField(auto_now_add=True)
    modificado = DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Adnetwork(TimeStampMixin):
    nombre = CharField(max_length=255, null=False)
    slug = SlugField(null=False, unique=True)
    reglas = JSONField(null=False, verbose_name="Reglas Adnetwork")
    accion = CharField(
        max_length=10,
        null=True,
        blank=True,
        default=None,
        choices=ACCIONES_POSTBACK,
        verbose_name="Accion Postback",
    )
    url = CharField(max_length=2048, null=True, blank=True, verbose_name="URL Postback")
    params = JSONField(null=True, verbose_name="Parametros Postback")
    activo = BooleanField(default=True)
    usuario = models.ManyToManyField(CoreUser)

    def __str__(self) -> str:
        return self.nombre

    def reglas(self) -> str:
        logger.info(pprint.pprint(self.reglas))
        return "hola"

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.nombre)
        return super().save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Adnetworks"


class Servicio(TimeStampMixin):
    nombre = CharField(max_length=64)
    slug = CharField(max_length=255, null=True, default=None)
    url = CharField(max_length=255, null=True, default=None, blank=True)
    activo = BooleanField(default=True)
    merchant_brokers = JSONField()
    moneda = CharField(max_length=3, default="CLP", choices=CURRENCY)
    url_ok = CharField(max_length=255, null=True, default=None, blank=True)
    url_nok = CharField(max_length=255, null=True, default=None, blank=True)

    def __str__(self) -> str:
        return self.nombre


class Tienda(TimeStampMixin):
    nombre = CharField(max_length=255)
    slug = SlugField(max_length=128, null=True, default=None)
    servicio = models.ForeignKey(Servicio, on_delete=models.DO_NOTHING)
    activo = BooleanField(default=True)
    dominio = CharField(max_length=255)
    google_tag = CharField(max_length=32, null=True, blank=True, default=None)
    pais = CharField(max_length=4, db_index=True, choices=PAISES, default="cl")
    notifica_compra = BooleanField(default=False)
    texto_titulo = CharField(max_length=128, null=True, blank=True, default=None)
    markdown_bajada = MDTextField(null=True, blank=True)
    markdown_pie = MDTextField(null=True, blank=True)

    def __str__(self) -> str:
        return self.nombre

    class Meta:
        verbose_name_plural = "Tiendas"


class Formulario(TimeStampMixin):
    nombre = CharField(max_length=255)
    fid = UUIDField(default=uuid.uuid4)
    servicio = models.ForeignKey(Servicio, on_delete=models.CASCADE)
    archivo_landing = CharField(max_length=255, default="skel/landing.html")
    archivo_checkout = CharField(max_length=255, default="skel/checkout.html")
    activo = BooleanField(default=True)
    dominio = CharField(max_length=255)
    google_ua = CharField(max_length=32, null=True, blank=True, default=None)
    google_tag = CharField(max_length=32, null=True, blank=True, default=None)
    matomo_id = CharField(max_length=32, null=True, blank=True, default=None)
    matomo_tag = CharField(max_length=32, null=True, blank=True, default=None)
    color = CharField(max_length=16, default="default", null=False)
    un_cliente_por_compra = BooleanField(default=False)
    descripcion = TextField(null=True, default=None, blank=True)
    correo_bienvenida = BooleanField(default=False)
    correo_compra = BooleanField(default=False)
    pais = CharField(max_length=4, db_index=True, choices=PAISES, default="cl")

    def __str__(self) -> str:
        return self.nombre


class Cliente(TimeStampMixin):
    user = models.ForeignKey(CoreUser, on_delete=models.SET_NULL, null=True, blank=True, default=None)
    servicio = models.ForeignKey(Servicio, on_delete=models.CASCADE)
    client_id = models.IntegerField(null=True, default=None, blank=True)
    pin = models.CharField(max_length=8, null=True, default=None, blank=True)

    def __str__(self) -> str:
        return f"{self.servicio}<{self.client_id}>"


class Producto(TimeStampMixin):
    formulario = models.ForeignKey(Formulario, on_delete=models.SET_NULL, null=True)
    tienda = models.ForeignKey(Tienda, on_delete=models.SET_NULL, null=True)
    nombre = CharField(max_length=100)
    slug = SlugField(null=True, unique=True, default=None)
    descripcion = TextField()
    valor = FloatField()
    moneda = CharField(max_length=3, default="CLP", choices=CURRENCY)
    activo = BooleanField(default=True)
    destacado = BooleanField(default=False)
    cambio = JSONField()

    def __str__(self) -> str:
        return self.nombre

    def obtiene_monto(self):
        if self.moneda == "CLP":
            return int(self.valor)
        else:
            return self.valor

    class Meta:
        verbose_name_plural = "Productos"


class Telefono(TimeStampMixin):
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE, null=True)
    #usuario = models.ForeignKey(CoreUser, on_delete=models.SET_NULL, null=True)
    servicio = models.ForeignKey(Servicio, on_delete=models.SET_NULL, null=True)
    numero = models.CharField(max_length=32)
    activo = models.BooleanField(default=True)

    def __str__(self) -> str:
        return self.numero

    class Meta:
        verbose_name_plural = "Telefonos"
        constraints = [models.UniqueConstraint(fields=["servicio", "numero"], name="telefono_unico")]

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(usuario=request.user)


class Correo(TimeStampMixin):
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE, null=True)
    #usuario = models.ForeignKey(CoreUser, on_delete=models.SET_NULL, null=True)
    servicio = models.ForeignKey(Servicio, on_delete=models.SET_NULL, null=True)
    correo = models.CharField(max_length=255)
    activo = models.BooleanField(default=True)

    def __str__(self) -> str:
        return self.correo

    class Meta:
        verbose_name_plural = "Correos"
        constraints = [models.UniqueConstraint(fields=["servicio", "correo"], name="correo_unico")]

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(usuario=request.user)


class Compra(TimeStampMixin):
    transaccion = models.CharField(max_length=255, default=uuid.uuid4)
    # usuario = models.ForeignKey(CoreUser, on_delete=models.SET_NULL, null=True)
    cliente = models.ForeignKey(Cliente, on_delete=models.SET_NULL, null=True, default=None, blank=True)
    broker = models.CharField(max_length=64)
    broker_pago_id = models.CharField(max_length=255, null=True, default=None, blank=True)
    producto = models.ForeignKey(Producto, on_delete=models.SET_NULL, null=True)
    nombre = models.CharField(max_length=255, null=True, blank=True, default=None)
    correo = models.CharField(max_length=255, null=True, blank=True, default=None)
    telefono = models.CharField(max_length=32, null=True, blank=True, default=None)
    estado = models.CharField(max_length=10, default="Pendiente", choices=ESTADOS_COMPRA)
    extra = JSONField(null=True, default=None, blank=True)
    url = models.CharField(max_length=2048, blank=True, null=True, default=None)
    broker_payload = JSONField(null=True, default=None, blank=True)
    broker_response = JSONField(null=True, default=None, blank=True)
    formulario = models.ForeignKey(Formulario, on_delete=models.SET_NULL, null=True)
    adnetwork = models.ForeignKey(Adnetwork, on_delete=models.SET_NULL, null=True, blank=True)
    es_promocion = models.BooleanField(default=False)

    def __str__(self) -> str:
        return str(self.transaccion)

    class Meta:
        verbose_name_plural = "Compras"

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(usuario=request.user)


class Mensaje(TimeStampMixin):
    compra = models.ForeignKey(Compra, on_delete=models.SET_NULL, null=True, blank=True, default=None)
    fecha_envio = DateTimeField(null=True, default=None, blank=True)
    tipo = models.CharField(max_length=10, default="email", choices=TIPOS_MENSAJE)
    estado = models.CharField(max_length=10, default="not_sent", choices=ESTADOS_MENSAJE)
    destino = models.CharField(max_length=2048, null=False)
    remitente = models.CharField(max_length=255, null=False)
    asunto = models.CharField(max_length=180, null=False)
    mensaje_texto = models.TextField(null=True, blank=True, default=None)
    mensaje_html = models.TextField(null=True, blank=True, default=None)
    mensaje_headers = JSONField(null=True, default=None, blank=True)
    resultado = models.TextField(null=True, blank=True, default=None)

    def __str__(self) -> str:
        return f"{self.tipo}({self.destino})"

    class Meta:
        verbose_name_plural = "Mensajes"


class Promocion(TimeStampMixin):
    nombre = models.CharField(null=False, max_length=255)
    slug = SlugField(null=False, unique=True, default=None)
    broker_slug = models.CharField(max_length=32, default=None, null=True, blank=True, choices=BROKER_SLUGS)
    activo = models.BooleanField(default=True)
    desde = models.DateTimeField(default=timezone.now)
    hasta = models.DateTimeField(null=True, blank=True)
    prints = models.IntegerField(default=0)
    clicks = models.IntegerField(default=0)
    compras = models.IntegerField(default=0)
    solo_nuevos = models.BooleanField(default=True)
    formulario = models.ForeignKey(Formulario, on_delete=models.SET_NULL, null=True)
    producto = models.ForeignKey(Producto, on_delete=models.SET_NULL, null=True)

    def __str__(self) -> str:
        return self.nombre

    class Meta:
        verbose_name_plural = "Promociones"
