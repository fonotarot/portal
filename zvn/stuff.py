import logging
import random
import shutil
import urllib
from typing import Any

import phonenumbers
from django.conf import settings
from django.core.signing import Signer
from django.db.models import Max
from django.db.models.query import QuerySet
from django.template.loader import render_to_string
from django.utils.text import slugify
from marshmallow import INCLUDE, ValidationError

from core.tasks import send_email, send_request
from zvn import firenze, merchants, models, schemas
from zvn.exceptions import HumanNeededException, NotMagicSortableException

logger = logging.getLogger("django")


def crear_pin(servicio_id: int) -> int:
    """
    crea y verifica un nuevo pin
    """
    pin = 0
    while True:
        pin = random.randint(11111111, 99999999)
        if not models.Cliente.objects.filter(pin=pin, servicio_id=servicio_id).first():
            break
    return pin


def crear_client_id(servicio_id: int) -> int:
    """
    crea y verifica un client_id
    """
    max_client_id = models.Cliente.objects.filter(servicio_id=servicio_id)
    cur_client_id = max_client_id.aggregate(Max("client_id"))
    new_client_id = 1 if not cur_client_id["client_id__max"] else (cur_client_id["client_id__max"] + 1)
    return new_client_id


def envia_correo_pin(info_compra: models.Compra) -> bool:
    datos = {
        "compra": info_compra,
        "asunto": f"Nuevo PIN para {info_compra.formulario.nombre}",
    }
    html_content = render_to_string(template_name="email/nuevo_pin.html", context=datos)
    text_content = render_to_string(template_name="email/nuevo_pin.txt", context=datos)

    mensaje = models.Mensaje()
    mensaje.compra = info_compra
    mensaje.remitente = "hola@fonotarot.cl"
    mensaje.destino = info_compra.correo
    mensaje.asunto = datos["asunto"]
    mensaje.mensaje_texto = text_content
    mensaje.mensaje_html = html_content
    mensaje.save()

    return True


def ejecuta_postback(compra) -> bool:
    data = {}
    if "url" not in compra.producto.formulario.postback:
        return False
    url = compra.producto.formulario.postback["url"]
    verb = "GET"
    if "verb" in compra.producto.formulario.postback:
        verb = compra.producto.formulario.postback["verb"]
    if "params" in compra.producto.formulario.postback:
        for param, name in compra.producto.formulario.postback["params"].items():
            if name.startswith("#") and name.endswith("#"):
                ###
                # TODO
                # This is a very stupid thing to do
                # REPLACE URGENT
                ###
                valor = eval(name[1:-1])
                data[param] = "".join(valor)
            else:
                data[param] = "".join(name)

    if verb == "GET":
        query_string = urllib.parse.urlencode(data)
        url = f"{url}?{query_string}"

    send_request.apply_async(args=[verb, url, None, None])

    return True


def parametros_postback(compra: models.Compra) -> dict:
    logger.info(f"Obteniendo parametros de {compra.adnetwork=}")
    data = {}
    if compra.adnetwork.params:
        for param, name in compra.adnetwork.params.items():
            if name.startswith("#") and name.endswith("#"):
                # Esto no deberia fallar, hay multiples chequeos previos
                # Pero igual creo que hay que verificar consistencia de datos
                # Tratar de buscar una alternativa mas segura a eval
                valor = eval(name[1:-1])
                data[param] = "".join(valor)
            else:
                data[param] = "".join(name)
    return data


def crea_htmls(formulario: Any, overwrite: bool = False) -> None:
    """
    copia los archivos del landing desde skel
    """
    logger.info("Copiando archivos html")
    if not overwrite:
        logger.info(f"Archivo: {formulario.archivo_landing} ya existe")

        logger.info(f"Archivo: {formulario.archivo_checkout} ya existe")

    shutil.copy(
        f"{settings.BASE_DIR}/zvn/templates/zvn/formularios/skel/landing.html",
        f"{settings.BASE_DIR}/zvn/templates/zvn/formularios/{formulario.archivo_landing}",
    )

    shutil.copy(
        f"{settings.BASE_DIR}/zvn/templates/zvn/formularios/skel/checkout.html",
        f"{settings.BASE_DIR}/zvn/templates/zvn/formularios/{formulario.archivo_checkout}",
    )


def get_host_for_real(request) -> str:
    """
    retorna el hostname del request
    """
    host = request.request.get_host()
    hostname = host.split(":")[0] if ":" in host else host
    logger.debug(f"Dominio encontrado: {hostname}")
    return hostname


def busca_formulario(hostname: str) -> models.Formulario | None:
    """
    retorna el Formulario para el hostname
    """
    logger.debug(f"Buscando formulario para: {hostname}")
    try:
        form = models.Formulario.objects.get(dominio__iexact=hostname, activo=1)
    except Exception:
        logger.warning(f"El dominio {hostname} no tiene formulario creado.")
        return None
    else:
        logger.debug(f"{form=}")
        return form


def busca_tienda(hostname: str) -> models.Tienda | None:
    """
    retorna la tienda para el hostname
    """
    logger.debug(f"Buscando tienda para: {hostname}")
    try:
        tienda = models.Tienda.objects.get(dominio__iexact=hostname, activo=1)
    except Exception:
        logger.warning(f"El dominio {hostname} no tiene tienda creada.")
        return None
    else:
        logger.debug(f"{tienda=}")
        return tienda


def lista_productos(formulario: models.Formulario) -> QuerySet:
    """
    retorna la lista de productos para el Formulario
    """
    return models.Producto.objects.filter(
        formulario=formulario.id,
        activo=1,
        moneda=formulario.servicio.moneda,
    )


def crea_pack(broker: str, producto: int) -> str:
    """
    retorna la cadena de verificacion para la compra
    """
    signer = Signer()
    purchase_pack = {"b": broker, "p": producto}
    sign_str = signer.sign_object(purchase_pack)
    return sign_str


def lee_pack(pack: str) -> Any:
    """
    retorna diccionario con datos del pack o false de lo contrario
    """
    signer = Signer()
    try:
        data = signer.unsign_object(pack)
    except Exception as error:
        logger.error(f"{error=}")
        return False
    return data


def busca_cliente(telefono: str = None, correo: str = None, servicio: Any = None) -> None | models.Cliente:
    """
    Busca Cliente en los sistemas
    """
    if not telefono and not correo and not servicio:
        raise Exception(f"Faltan parametros: {telefono=}, {correo=}, {servicio=}")

    portal = busca_cliente_portal(correo, telefono, servicio)
    if portal:
        return portal
    logger.info(f"No existe {telefono=}, {correo=} en {servicio=}.")
    return None


def busca_cliente_portal(correo: str, telefono: str, servicio: Any) -> models.Cliente | None:
    """
    Busca Cliente en Portal
    """
    logger.info(f"Vamos a buscar en Portal")
    correo_registrado = models.Correo.objects.filter(correo=correo, servicio=servicio)
    telefono_registrado = models.Telefono.objects.filter(numero=telefono, servicio=servicio)
    if telefono_registrado and correo_registrado:
        telefono_reg = telefono_registrado.get()
        correo_reg = correo_registrado.get()
        if telefono_reg.cliente == correo_reg.cliente:
            return telefono_reg.cliente
        else:
            # sorting_hat
            return None
    elif telefono_registrado and not correo_registrado:
        telefono_reg = telefono_registrado.get()
        return telefono_reg.cliente
    elif not telefono_registrado and correo_registrado:
        correo_reg = correo_registrado.get()
        return correo_reg.cliente
    else:
        # sorting_hat
        return None


def crea_cliente_portal(telefono: Any, correo: Any, nombre: str, servicio: Any, cliente: Any = None) -> models.Cliente:
    """
    Crea un cliente en Portal
    """
    telefonos = []
    correos = []
    if isinstance(telefono, str):
        telefonos.append(telefono)
    if isinstance(telefono, list):
        telefonos = telefono
    if cliente and "telefonos" in cliente:
        for tel in cliente["telefonos"]:
            telefonos.append(f"{tel}")
    if cliente and "phone_number" in cliente:
        telefonos.append(f"{cliente['phone_number']}")
    if isinstance(correo, str):
        correos.append(correo)
    if cliente and "correo" in cliente:
        correos.append(cliente["correo"])
    if cliente and "email" in cliente:
        correos.append(cliente["email"])

    if cliente and "clientid" in cliente:
        clientid = cliente["clientid"]
    elif cliente and "client_id" in cliente:
        clientid = cliente["client_id"]
    else:
        raise Exception("No deberiamos llegar aqui stuff.crea_cliente_portal()")

    if cliente and "pin" in cliente:
        pin = cliente["pin"]
    elif cliente and "pin_code" in cliente:
        pin = cliente["pin_code"]
    else:
        pin = crear_pin(servicio.id)

    nuevo_cliente = models.Cliente.objects.create(servicio=servicio, pin=pin, client_id=clientid)
    logger.info(f"{nuevo_cliente=}")
    for t in telefonos:
        try:
            models.Telefono.objects.create(cliente=nuevo_cliente, numero=t, servicio=servicio)
        except Exception as e:
            logger.warn(f"{nuevo_cliente=} ya tiene el telefono {t=} en {servicio=}")
    for c in correos:
        try:
            models.Correo.objects.create(cliente=nuevo_cliente, correo=c, servicio=servicio)
        except Exception as e:
            logger.warn(f"{nuevo_cliente=} ya tiene el correo {c=} en {servicio=}")
    return nuevo_cliente


def busca_cliente_audiotex(slug: str, correo: str, telefono: int) -> dict | None:
    """
    Busca Cliente en Audiotex
    """
    logger.info(f"Vamos a buscar en Audiotex({slug})")
    api = merchants.Audiotex()
    try:
        existe = api.buscar_usuario(slug, correo, telefono)
    except Exception as e:
        logger.warn(e)
        return None
    else:
        return existe.json()


def crea_cliente_audiotex(telefono: Any, correo: Any, nombre: str, servicio: Any, creditos: int = 0):
    """
    Crea un cliente en Audiotex
    """
    logger.info(f"Vamos a crear un cliente en Audiotex({servicio.slug})")
    api = merchants.Audiotex()
    try:
        payload = {"creditos": creditos, "telefonos": [telefono], "correo": correo}
        logger.info(f"{payload=}")
        nuevo = api.crear_cliente(servicio.slug, payload)
    except Exception as e:
        logger.warn(e)
        return None
    else:
        return nuevo.json()


def crea_cliente_firenze(telefono: Any, correo: Any, nombre: str, servicio: Any, creditos: int = 0) -> dict | None:
    """
    Crea un cliente en Audiotex
    """
    logger.info(f"Vamos a crear un cliente en Firenze({servicio.slug})")
    api = firenze.TarotFirenze()
    try:
        payload = {"credits": creditos, "phone_number": telefono, "email": correo}
        logger.info(f"{payload=}")
        nuevo = api.crear_cliente(servicio.slug, payload)
    except Exception as e:
        logger.warn(e)
        return None
    else:
        return nuevo.json()


def busca_cliente_firenze(slug: str, correo: str, telefono: str) -> dict | None:
    """
    Busca Cliente en Firenze
    """
    logger.info(f"Vamos a buscar en Firenze({slug})")
    return None


def crea_correos_notificacion_compra(info_compra: models.Compra) -> None:
    datos = {
        "compra": info_compra,
        "asunto": f"Nueva Compra en {info_compra.formulario.nombre} - {info_compra.transaccion}",
    }
    html_content = render_to_string(template_name="email/nueva_compra.html", context=datos)
    text_content = render_to_string(template_name="email/nueva_compra.txt", context=datos)
    for admin in settings.ADMINS:
        mensaje = models.Mensaje()
        mensaje.compra = info_compra
        mensaje.remitente = "hola@fonotarot.cl"
        mensaje.destino = admin[1]
        mensaje.asunto = datos["asunto"]
        mensaje.mensaje_texto = text_content
        mensaje.mensaje_html = html_content
        mensaje.save()

    return None


def crea_correos_notificacion_compra_a_cliente(info_compra: models.Compra) -> None:
    datos = {
        "compra": info_compra,
        "asunto": f"Gracias por tu compra en {info_compra.formulario.nombre}",
    }
    html_content = render_to_string(template_name="email/nueva_compra_notificacion_con_pin.html", context=datos)
    text_content = render_to_string(template_name="email/nueva_compra_notificacion_con_pin.txt", context=datos)
    for admin in settings.ADMINS:
        mensaje = models.Mensaje()
        mensaje.compra = info_compra
        mensaje.remitente = "hola@fonotarot.cl"
        mensaje.destino = info_compra.correo
        mensaje.asunto = datos["asunto"]
        mensaje.mensaje_texto = text_content
        mensaje.mensaje_html = html_content
        mensaje.save()

    return None


def sorting_hat(payload: Any) -> Any:
    """
    Para definir cliente/client_id de Portal.

    en CRM se llama normalizaClientId
    """
    if "telefono" in payload:
        tel = phonenumbers.parse(f"+{payload['telefono']}", None)
    if "correo" in payload:
        correo = payload["correo"]
    if "servicio" in payload:
        servicio = payload["servicio"]
        slug = servicio.slug

    audiotex = None
    correo_registrado = models.Correo.objects.filter(correo=correo, servicio=servicio)
    telefono_registrado = models.Telefono.objects.filter(numero=tel.raw_input, servicio=servicio)
    if telefono_registrado and correo_registrado:
        if telefono_registrado.cliente != correo_registrado.cliente:
            raise HumanNeededException("Telefono y Correo asignados a distinto cliente")
    elif telefono_registrado or correo_registrado:
        raise NotMagicSortableException("Se encontró un telefono o un correo, terminamos.")
    else:
        # cuando no hay nada, sospecha de cliente nuevo
        # buscamos en audiotex
        audiotex = busca_cliente_audiotex(slug, correo, tel.national_number)

    if audiotex:
        if "clientid" in audiotex:
            logger.info(f"Se encontró el client_id: {audiotex['clientid']}")
            busca = models.Cliente.objects.filter(client_id=audiotex["clientid"], servicio=servicio)
            if busca:
                encontrado = busca.get()
                logger.info(f"Se encontró el Cliente: {encontrado=}, asignar a compra")
                logger.info(f"return encontrado, False")
                return encontrado, False
            else:
                logger.info(f"No se encontró client_id en portal, crear cliente")
                logger.info(f"return audiotex, False")
                return audiotex, False
        else:
            raise HumanNeededException("Se encontró registro en audiotex, pero no clientid")

    logger.info(f"No se encontró registro, crear cliente")
    logger.info(f"return None, True")
    return None, True


def asigna_cliente(compra, crear: bool = True):
    """
    Para definir cliente/client_id de Portal.

    """
    payload = {
        "telefono": compra.telefono,
        "correo": compra.correo,
        "servicio": compra.formulario.servicio,
    }
    logger.info(f"llamando a sorting_hat({payload=})")
    cliente, nuevo = sorting_hat(payload=payload)

    if cliente and not nuevo:
        if not isinstance(cliente, models.Cliente):
            logger.info(f"crea_cliente_portal()")
            nuevo_cliente = crea_cliente_portal(
                compra.telefono,
                compra.correo,
                compra.nombre,
                compra.formulario.servicio,
                cliente,
            )
            cliente = nuevo_cliente
        compra.cliente = cliente
        compra.save(update_fields=["cliente"])
    if not cliente and nuevo and crear:
        # creamos en audiotex y luego en portal
        logger.info(f"crea_cliente_audiotex()")
        nuevo_cliente_audiotex = crea_cliente_audiotex(
            compra.telefono, compra.correo, compra.nombre, compra.formulario.servicio
        )
        logger.info(f"crea_cliente_portal()")
        nuevo_cliente = crea_cliente_portal(
            compra.telefono,
            compra.correo,
            compra.nombre,
            compra.formulario.servicio,
            nuevo_cliente_audiotex,
        )
        compra.cliente = nuevo_cliente
        compra.save(update_fields=["cliente"])

    return compra


def reglas_uri() -> dict:
    return {
        "fonotarot": schemas.Fonotarot,
    }


def buscar_adnetwork(
    info_compra: models.Compra,
    asignar: bool = True,
) -> models.Adnetwork | None:
    logger.info(f"Buscando ADNetwork para {info_compra}")
    if not info_compra.extra or info_compra.adnetwork:
        logger.info(f"{info_compra.extra=} or {info_compra.adnetwork=}")
        return info_compra.adnetwork

    reglas = reglas_uri()
    selected = False
    for name, network in reglas.items():
        validation_errors = network(unknown=INCLUDE).validate(info_compra.extra)
        if not validation_errors:
            selected = True
            selected_name = name
            selected_network = network  # pragma: noqa
            ## No estoy seguro de esto
            ## Quizá deberia quedarse con el primero que encuentre
            # break
    if not selected:
        logger.warning("No se encontro ADNetwork.")
        return None

    try:
        adnetwork = models.Adnetwork.objects.get(slug=selected_name)
    except Exception as e:
        logger.warning(f"{selected_name=} no existe en Adnetwork()")
        return None

    if not asignar:
        logger.info(f"{adnetwork=}, {asignar=}")
        return adnetwork

    info_compra.adnetwork = adnetwork
    info_compra.save(update_fields=["adnetwork"])
    logger.info(f"{adnetwork=}, {asignar=}")
    return info_compra.adnetwork
