# Generated by Django 5.0.6 on 2024-06-08 01:49

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("zvn", "0051_delete_payment"),
    ]

    operations = [
        migrations.CreateModel(
            name="Tienda",
            fields=[
                ("id", models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                ("creado", models.DateTimeField(auto_now_add=True)),
                ("modificado", models.DateTimeField(auto_now=True)),
                ("nombre", models.CharField(max_length=255)),
                ("slug", models.CharField(default=None, max_length=255, null=True)),
                ("activo", models.BooleanField(default=True)),
                ("dominio", models.CharField(max_length=255)),
                ("descripcion", models.TextField(blank=True, default=None, null=True)),
                ("google_tag", models.CharField(blank=True, default=None, max_length=32, null=True)),
                (
                    "pais",
                    models.CharField(
                        choices=[("cl", "Chile"), ("auto", "Autodetectar")], db_index=True, default="cl", max_length=4
                    ),
                ),
                ("notifica_compra", models.BooleanField(default=False)),
                ("servicio", models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to="zvn.servicio")),
            ],
            options={
                "verbose_name_plural": "Tiendas",
            },
        ),
    ]
