from django.urls import path

from zvn import views, tienda
from zvn.tarot import Landing, ProcessPayment, Promociones, WP_Checkout
from zvn.users import user_portal

urlpatterns = [
    path("", Landing.as_view(), name="landing"),
    path("tienda/<slug:tienda>", tienda.landing, name="tiendas"),
    path("tienda/<slug:tienda>/<slug:producto>", tienda.CompraProducto.as_view(), name="producto"),
    path("la.estrella", Promociones.as_view(), name="promociones"),
    path("el.mago", Promociones.as_view(), name="promociones"),
    path("la.emperatriz", Promociones.as_view(), name="promociones"),
    path("as.de.oros", Promociones.as_view(), name="promociones"),
    path("la.rueda.de.la.fortuna", Promociones.as_view(), name="promociones"),
    path("el.sol", Promociones.as_view(), name="promociones"),
    path("el.mundo", Promociones.as_view(), name="promociones"),
    path("users/", user_portal.urls),
    path(
        "notifica/<uuid:transaccion>",
        views.notificacion_merchants,
        name="notificacion_merchants",
    ),
    path("checkout/<slug:origin>", WP_Checkout.as_view(), name="wp_checkout"),
    path(
        "payment/<slug:servicio>/<uuid:formulario>",
        ProcessPayment.as_view(),
        name="process_payment",
    ),
]
