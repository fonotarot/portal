# Generated by Django 4.1.4 on 2022-12-07 20:52

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("zvn", "0044_alter_cliente_pin"),
    ]

    operations = [
        migrations.AlterField(
            model_name="cliente",
            name="pin",
            field=models.CharField(blank=True, default=None, max_length=8, null=True),
        ),
    ]
