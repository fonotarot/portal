class NotMagicSortableException(Exception):
    pass


class HumanNeededException(Exception):
    pass
