# Generated by Django 4.1.3 on 2022-12-06 00:40

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("zvn", "0040_remove_promocion_servicio_promocion_formulario_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="promocion",
            name="producto",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to="zvn.producto",
            ),
        ),
    ]
