import json
import logging
import uuid
import time
import requests
from celery import shared_task
from django.conf import settings
from django.core import serializers
from django.core.mail import EmailMultiAlternatives
from django.utils import timezone

from zvn import firenze, merchants
from zvn.models import Cliente, Compra, Correo, Mensaje, Telefono
from zvn.stuff import (
    asigna_cliente,
    busca_cliente,
    buscar_adnetwork,
    crea_cliente_audiotex,
    crea_cliente_firenze,
    crea_cliente_portal,
    crea_correos_notificacion_compra,
    crea_correos_notificacion_compra_a_cliente,
    crear_pin,
    envia_correo_pin,
    parametros_postback,
    sorting_hat,
)
from core.tasks import send_request

logger = logging.getLogger("django")


@shared_task(serializer="json")
def nueva_compra(compra: uuid.UUID) -> dict:
    """
    Procesos a realizar post-creacion de compra
    """
    logger.info(f"Iniciando post-procesos de Compra<{compra}>")
    try:
        info_compra = Compra.objects.filter(transaccion=compra).get()
    except Exception as e:
        raise Exception(f"Compra<{compra}> no existe")

    if not info_compra.formulario.un_cliente_por_compra:
        try:
            cliente = busca_cliente(
                info_compra.telefono,
                info_compra.correo,
                info_compra.formulario.servicio,
            )
        except Exception as e:
            raise Exception(e)
        logger.info(f"busca_cliente: {cliente=}")

        if cliente:
            info_compra.cliente = cliente
            info_compra.save(update_fields=["cliente"])
        else:
            # Si falla, queremos registrarlo, asi que que falle nomas, lo captará celery
            asigna_cliente(info_compra, False)
    else:
        # No queremos crear el cliente al momento de la creacion de la compra
        # Asi que movemos esto a procesar_pago
        # Creamos cliente con datos del formulario, primero en audiotex
        # para que nos de el pin
        # En paralero implementar en firenze
        pass

    # Buscamos el adnetwork si tiene parametros
    if info_compra.extra:
        adnetwork = buscar_adnetwork(info_compra=info_compra, asignar=False)
        info_compra.adnetwork = adnetwork
        info_compra.save(update_fields=["adnetwork"]) if adnetwork else None

    logger.info(f"{info_compra=}")

    ### Notificaciones de compra a admines
    crea_correos_notificacion_compra(info_compra=info_compra)

    ### Notificacion de nueva cuenta de cliente (relacionado con sorting_hat)
    if info_compra.formulario.correo_bienvenida:
        ## Solamente cuando se procesa el pago
        pass

    ### Notificacion de nueva compra a cliente
    if info_compra.formulario.correo_compra:
        # aqui no va, va en procesar pago
        pass
    return {"compra": f"{info_compra=}"}


@shared_task
def envia_correo(mensaje_id: int = None) -> dict:
    """
    Usar este mejor
    recibe el id del mensaje para que el proceso se encargue de la actualizacion
    Si se desea enviar un correo async sin ser procesado como mensaje, usar:
    core.tasks.send_email
    """
    if not mensaje_id:
        raise Exception(f"{mensaje_id=}")

    mensaje = Mensaje.objects.filter(id=mensaje_id)
    if mensaje:
        msg = mensaje.get()

    if msg.estado != "not_sent":
        raise Exception(f"Mensaje con estado {msg.estado}, saliendo.")

    msg.estado = "sending"
    msg.save()
    logger.info(f"Enviando correo a: {msg.destino}")
    email = EmailMultiAlternatives(
        subject=f"{settings.EMAIL_SUBJECT_PREFIX}{msg.asunto}",
        body=msg.mensaje_texto,
        to=[msg.destino],
        headers=msg.mensaje_headers,
    )
    email.attach_alternative(msg.mensaje_html, "text/html")
    try:
        email.send(fail_silently=False)
    except Exception as e:
        msg.estado = "rejected"
        msg.fecha_envio = timezone.now()
        msg.resultado = e
        msg.save()
        raise Exception(f"No se pudo enviar el correo: {e}")
    else:
        logger.info(f"Correo Enviado!")
        msg.estado = "sent"
        msg.fecha_envio = timezone.now()
        msg.save()
        return {"tipo": msg.tipo, "destino": msg.destino, "fecha": msg.fecha_envio}


@shared_task
def procesa_pago(transaccion: uuid.UUID):
    """
    Procesos a realizar post-creacion de compra
    """
    logger.info(f"Procesando pago de Compra<{transaccion}>")
    try:
        compra = Compra.objects.filter(transaccion=transaccion).first()
    except Exception as e:
        raise Exception(f"Compra<{compra}> no existe")

    segundos = int(compra.producto.cambio["segundos"])
    logger.info(f"Vamos a sumar {segundos} segundos")

    if not compra.cliente:
        if compra.formulario.un_cliente_por_compra:
            # Creamos cliente con datos del formulario, primero en audiotex
            # para que nos de el pin
            # En paralero implementar en firenze
            cliente_audiotex = crea_cliente_audiotex(
                compra.telefono,
                compra.correo,
                compra.nombre,
                compra.formulario.servicio,
            )
            cliente_firenze = crea_cliente_firenze(
                compra.telefono,
                compra.correo,
                compra.nombre,
                compra.formulario.servicio,
            )
            cliente = crea_cliente_portal(
                compra.telefono,
                compra.correo,
                compra.nombre,
                compra.formulario.servicio,
                cliente_audiotex,
            )
            logger.info(f"Nuevo Cliente: {cliente=}")
            compra.cliente = cliente
            compra.save(update_fields=["cliente"])
        else:
            # Proceso de busqueda
            cliente = busca_cliente(
                compra.telefono,
                compra.correo,
                compra.formulario.servicio,
            )
            logger.info(f"busca_cliente: {cliente=}")

            if cliente:
                compra.cliente = cliente
                compra.save(update_fields=["cliente"])
            else:
                # Si falla, queremos registrarlo, asi que que falle nomas, lo captará celery
                compra = asigna_cliente(compra, True)

    logger.info(f"client_id: {compra.cliente.client_id=}")
    if not compra.cliente.client_id:
        raise Exception("No se pudo crear el client_id, help.")

    if compra.adnetwork and compra.adnetwork.accion:
        # Agregar URL Query en destino
        params = parametros_postback(compra) or None
        mensaje = Mensaje.objects.create(
            tipo="url",
            compra=compra,
            remitente="hola@fonotarot.cl",
            destino=compra.adnetwork.url,
            asunto=compra.adnetwork.accion,
            mensaje_texto=params,
        )

    envia_evento_purchase.apply_async(
        kwargs={
            "item_name": compra.producto.nombre,
            "item_id": compra.producto.slug,
            "item_price": compra.producto.valor,
            "item_currency": compra.producto.moneda,
            "transaccion": compra.transaccion,
            "client_id": compra.cliente.client_id,
        },
        countdown=2,
    )

    api = merchants.Audiotex()
    saldo = api.sumar_saldo(
        slug=compra.producto.formulario.servicio.slug,
        client_id=compra.cliente.client_id,
        segundos=segundos,
    )
    compra.estado = "Pagado"
    compra.save(update_fields=["estado"])

    ### Notificaciones de compra a admines
    crea_correos_notificacion_compra(info_compra=compra)

    # Notificaciones a clientes movidas a procesa_pago_firenze

    return saldo.json()


@shared_task(serializer="json")
def envia_evento_purchase(
    item_id: str, item_name: str, item_price: str, item_currency: str, transaccion: str, client_id: str
):
    """
    Intencionalmente se usará requests directamente ya que es un proceso en si mismo.
    """

    payload = {
        "client_id": f"{uuid.uuid4()}",
        "user_id": f"{client_id}",
        "timestamp_micros": time.time_ns(),
        "non_personalized_ads": False,
        "events": [
            {
                "name": "purchase",
                "params": {
                    "items": [
                        {
                            "index": 0,
                            "item_name": f"{item_name}",
                            "quantity": 1,
                            "item_brand": "Fonotarot",
                            "item_category": "Tarot Prepago",
                            "price": f"{item_price}",
                            "currency": f"{item_currency}",
                            "item_id": f"{item_id}",
                        }
                    ],
                    "currency": f"{item_currency}",
                    "transaction_id": f"{transaccion}",
                    "value": f"{item_price}",
                },
            }
        ],
    }
    logger.info(f"{payload = }")
    debug_req = requests.post(
        settings.GA_DEBUG_URL,
        json=payload,
        headers={
            "x-application-name": "fonotarot/portal-2024.10.18",
        },
    )
    logger.info(f"{debug_req.status_code = } - {debug_req.json() = }")

    """yolo! Lo enviamos y no nos interesa que pase."""
    r = requests.post(
        settings.GA_URL,
        json=payload,
        headers={
            "x-application-name": "fonotarot/portal-2024.10.18",
        },
    )

    return [r.status_code, payload]


@shared_task
def procesa_pago_firenze(transaccion: uuid.UUID):
    """
    Procesos a realizar post-creacion de compra para Firenze
    """
    logger.info(f"Procesando pago de Compra<{transaccion}>")
    try:
        compra = Compra.objects.filter(transaccion=transaccion).first()
    except Exception as e:
        raise Exception(f"Compra<{compra}> no existe")

    segundos = int(compra.producto.cambio["segundos"])
    logger.info(f"Vamos a sumar {segundos} segundos")

    if not compra.cliente:
        if compra.formulario.un_cliente_por_compra:
            cliente_firenze = crea_cliente_firenze(
                compra.telefono,
                compra.correo,
                compra.nombre,
                compra.formulario.servicio,
            )
            cliente = crea_cliente_portal(
                compra.telefono,
                compra.correo,
                compra.nombre,
                compra.formulario.servicio,
                cliente_firenze,
            )
            logger.info(f"Nuevo Cliente: {cliente=}")
            compra.cliente = cliente
            compra.save(update_fields=["cliente"])
        else:
            # Proceso de busqueda
            cliente = busca_cliente(
                compra.telefono,
                compra.correo,
                compra.formulario.servicio,
            )
            logger.info(f"busca_cliente: {cliente=}")

            if cliente:
                compra.cliente = cliente
                compra.save(update_fields=["cliente"])
            else:
                # Si falla, queremos registrarlo, asi que que falle nomas, lo captará celery
                compra = asigna_cliente(compra, True)

    logger.info(f"client_id: {compra.cliente.client_id=}")
    if not compra.cliente.client_id:
        raise Exception("No se pudo crear el client_id, help.")

    if compra.adnetwork and compra.adnetwork.accion:
        # Agregar URL Query en destino
        params = parametros_postback(compra) or None
        mensaje = Mensaje.objects.create(
            tipo="url",
            compra=compra,
            remitente="hola@fonotarot.cl",
            destino=compra.adnetwork.url,
            asunto=compra.adnetwork.accion,
            mensaje_texto=params,
        )

    api = firenze.TarotFirenze()
    saldo = api.sumar_saldo(
        slug=compra.producto.formulario.servicio.slug,
        client_id=compra.cliente.client_id,
        segundos=segundos,
    )
    compra.estado = "Pagado"
    compra.save(update_fields=["estado"])

    ### Notificaciones de compra a admines
    crea_correos_notificacion_compra(info_compra=compra)

    # Correo bienvenida usuario (PIN)
    # Cambiar este parametro en los modelos
    # Deberia llamarse "correo_pin" o algo así
    # if compra.formulario.correo_bienvenida:
    #     envia_correo_pin(info_compra=compra)
    # Cambiar esto por un correo de bienvenida
    # como la gente indicando que existe un portal y esas cosas

    ### Correo confirmacion pago
    if compra.formulario.correo_compra:
        crea_correos_notificacion_compra_a_cliente(info_compra=compra)

    return saldo.json()


@shared_task
def envia_url(mensaje_id: int = None) -> dict:
    """
    Usar este mejor
    recibe el id del mensaje para que el proceso se encargue de la actualizacion
    Si se desea enviar un curl sin ser procesado como mensaje, usar:
    core.tasks.send_request
    """
    if not mensaje_id:
        raise Exception(f"{mensaje_id=}")

    mensaje = Mensaje.objects.filter(id=mensaje_id)
    if mensaje:
        msg = mensaje.get()

    if msg.estado != "not_sent":
        raise Exception(f"Mensaje con estado {msg.estado}, saliendo.")

    msg.estado = "sending"
    msg.save()
    params = json.loads(msg.mensaje_texto.replace("'", '"')) if msg.mensaje_texto else None
    data = json.loads(msg.mensaje_html.replace("'", '"')) if msg.mensaje_html else None

    logger.info(f"{msg.asunto=} {msg.destino=} {params=} {data=} {msg.mensaje_headers=}")
    llamada = requests.request(
        msg.asunto,
        msg.destino,
        params=params,
        data=data,
        headers=msg.mensaje_headers,
    )
    if llamada.status_code not in [200, 201, 301, 302]:
        msg.estado = "rejected"
    else:
        msg.estado = "sent"

    msg.fecha_envio = timezone.now()
    msg.resultado = {
        "status": llamada.status_code,
        "headers": llamada.headers,
        "data": llamada.text,
    }
    msg.save()
    return {
        "verb": msg.asunto,
        "url": llamada.url,
        "status_code": llamada.status_code,
    }


def envia_mensaje(mensaje: Mensaje) -> bool:
    """
    procesa el envio de un mensaje
    """
    if mensaje.tipo == "email":
        logger.info(f"envia_correo.apply_async(kwargs={{mensaje_id: {mensaje.id}}}, countdown=5)")
        envia_correo.apply_async(kwargs={"mensaje_id": mensaje.id}, countdown=5)
        return True
    if mensaje.tipo == "url":
        logger.info(f"envia_url.apply_async(kwargs={{mensaje_id: {mensaje.id}}}, countdown=5)")
        envia_url.apply_async(kwargs={"mensaje_id": mensaje.id}, countdown=5)
        return True
    elif mensaje.tipo == "email_sync":
        logger.info(f"envia_correo(mensaje_id={mensaje.id})")
        envia_correo(mensaje_id=mensaje.id)
        return True
    elif mensaje.tipo == "sms":
        pass
    elif mensaje.tipo == "voz":
        pass
    elif mensaje.tipo == "whatsapp":
        pass
    elif mensaje.tipo == "telegram":
        pass
    elif mensaje.tipo == "webpush":
        pass
    return False
