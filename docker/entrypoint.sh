#!/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

echo "Portal Starting"
poetry run gunicorn --name portal -b 0.0.0.0:80 -w 4 --forwarded-allow-ips=* cthulhu.wsgi:application

exit 0