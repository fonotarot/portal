import logging

from django.contrib.admin import AdminSite, ModelAdmin
from django.contrib.admin.forms import AuthenticationForm

from zvn.models import Adnetwork, Compra, Mensaje

logger = logging.getLogger("django")


class UserPortal(AdminSite):
    login_form = AuthenticationForm

    def has_permission(self, request):
        return request.user.is_active and (
            request.user.groups.filter(name="adnetwork").exists()
            or request.user.is_staff
        )


class ZvnAdmin(ModelAdmin):
    readonly_fields = ("creado", "modificado")


class FiltroAdnetworks(ZvnAdmin):
    list_display = ("nombre", "slug", "accion", "url", "activo")

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(usuario=request.user)


class FiltroCompras(ZvnAdmin):
    list_display = ("adnetwork", "formulario", "producto", "correo", "estado")
    fields = [
        "adnetwork",
        "formulario",
        "producto",
        "correo",
        "nombre",
        "estado",
        "extra",
        "creado",
        "modificado",
    ]

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(adnetwork__usuario=request.user, estado="Pagado")


class FiltroMensaje(ZvnAdmin):
    list_display = ("tipo", "estado", "destino")

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(compra__adnetwork__usuario=request.user, tipo="url")


user_portal = UserPortal(name="user_portal")
user_portal.register(Adnetwork, FiltroAdnetworks)
user_portal.register(Compra, FiltroCompras)
# user_portal.register(Mensaje, FiltroMensaje)
