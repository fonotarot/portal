from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError, JsonResponse
from zvn.models import Tienda, Producto

import logging
from django.views.generic import View
from django.shortcuts import redirect, render

import markdown

logger = logging.getLogger("django")


def landing(request, tienda):
    info = Tienda.objects.get(slug=tienda)
    productos = Producto.objects.filter(tienda=info).order_by("valor").all()

    datos = {
        "tienda": tienda,
        "info": info,
        "productos": productos,
        "bajada": markdown.markdown(info.markdown_bajada),
        "pie": markdown.markdown(info.markdown_pie),
    }
    return render(request, f"zvn/tienda/landing-applab.html", datos)


class CompraProducto(View):
    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)

    def get(self, request, *args, **kwargs) -> HttpResponse | HttpResponseRedirect:
        parametros = kwargs

        info = Tienda.objects.get(slug=parametros["tienda"])
        datos = {
            "tienda": parametros["tienda"],
            "producto": Producto.objects.get(slug=parametros["producto"]),
            "info": info,
        }
        return render(request, f"zvn/tienda/checkout.html", datos)

    def post(self, request, *args, **kwargs) -> HttpResponse | HttpResponseRedirect:
        print(f"{request.POST.get('broker')=}")
        return HttpResponseServerError("No se pudo crear el pago")
