# Generated by Django 4.1.8 on 2023-04-11 04:09

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("zvn", "0049_payment_alter_producto_moneda_alter_servicio_moneda"),
    ]

    operations = [
        migrations.AlterField(
            model_name="adnetwork",
            name="params",
            field=models.JSONField(null=True, verbose_name="Parametros Postback"),
        ),
        migrations.AlterField(
            model_name="compra",
            name="broker_payload",
            field=models.JSONField(blank=True, default=None, null=True),
        ),
        migrations.AlterField(
            model_name="compra",
            name="broker_response",
            field=models.JSONField(blank=True, default=None, null=True),
        ),
        migrations.AlterField(
            model_name="compra",
            name="extra",
            field=models.JSONField(blank=True, default=None, null=True),
        ),
        migrations.AlterField(
            model_name="mensaje",
            name="mensaje_headers",
            field=models.JSONField(blank=True, default=None, null=True),
        ),
        migrations.AlterField(
            model_name="producto",
            name="cambio",
            field=models.JSONField(),
        ),
        migrations.AlterField(
            model_name="servicio",
            name="merchant_brokers",
            field=models.JSONField(),
        ),
    ]
