import logging
from dataclasses import dataclass, field
from typing import List

import requests
from django.conf import settings

logger = logging.getLogger("django")


@dataclass
class FirenzeAPI:
    api_url: str = settings.FIRENZE_API_URL
    api_user: str = settings.FIRENZE_API_USER
    api_pass: str = settings.FIRENZE_API_PASSWORD
    api_scopes: str = settings.FIRENZE_API_SCOPES
    api_token: str = None

    def get_token(self):
        if not self.api_token:
            data = {
                "grant_type": "password",
                "scope": self.api_scopes,
                "username": self.api_user,
                "password": self.api_pass,
            }
            request = requests.post(f"{self.api_url}/token", data=data)
            request.raise_for_status()
            datos = request.json()
            self.api_token = datos["access_token"]
        return self.api_token


@dataclass
class TarotFirenze(FirenzeAPI):
    def buscar_usuario(self, slug, correo, telefono):
        payload = {"correo": correo, "telefono": telefono}

        busca = requests.get(
            f"{self.api_url}/tarot/{slug}/client",
            params=payload,
            headers={
                "accept": "application/json",
                "Authorization": f"Bearer {self.get_token()}",
            },
        )
        busca.raise_for_status()
        return busca

    def sumar_saldo(self, slug: str, client_id: int, segundos: int):
        payload = {"credits": segundos}
        saldo = requests.put(
            f"{self.api_url}/tarot/{slug}/{client_id}",
            json=payload,
            headers={
                "accept": "application/json",
                "Authorization": f"Bearer {self.get_token()}",
            },
        )
        print(f"{saldo.url=}")
        print(f"{payload=}")
        saldo.raise_for_status()

        return saldo

    def crear_cliente(self, slug: str, payload: dict):
        cliente = requests.post(
            f"{self.api_url}/tarot/{slug}",
            json=payload,
            headers={
                "accept": "application/json",
                "Authorization": f"Bearer {self.get_token()}",
            },
        )
        if cliente.status_code != 200:
            logger.error(cliente.json())
        cliente.raise_for_status()
        return cliente
