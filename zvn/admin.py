import logging

from django.contrib import admin, messages
from django.utils.translation import ngettext

from zvn.models import (
    Adnetwork,
    Cliente,
    Compra,
    Correo,
    Formulario,
    Mensaje,
    Producto,
    Promocion,
    Servicio,
    Telefono,
    Tienda,
)
from zvn.stuff import (
    asigna_cliente,
    buscar_adnetwork,
    crea_correos_notificacion_compra,
    crea_correos_notificacion_compra_a_cliente,
    envia_correo_pin,
)
from zvn.tasks import envia_mensaje, procesa_pago, procesa_pago_firenze

logger = logging.getLogger("django")


class ZvnAdmin(admin.ModelAdmin):
    readonly_fields = ("creado", "modificado")


class FiltroServicio(ZvnAdmin):
    list_display = (
        "nombre",
        "slug",
        "url_ok",
        "moneda",
    )


class FiltroProducto(ZvnAdmin):
    list_display = ("nombre", "formulario", "tienda", "moneda", "valor", "cambio")


class FiltroCliente(ZvnAdmin):
    list_display = ("client_id", "servicio", "user")
    list_per_page = 50


class FiltroTelefono(ZvnAdmin):
    list_display = ("numero", "servicio", "cliente", "activo")
    list_per_page = 20


class FiltroCorreo(ZvnAdmin):
    list_display = ("correo", "servicio", "cliente", "activo")
    list_per_page = 20


class FiltroFormulario(ZvnAdmin):
    list_display = (
        "nombre",
        "servicio",
        "dominio",
        "activo",
        "google_ua",
        "matomo_id",
        "pais",
    )


class FiltroTienda(ZvnAdmin):
    list_display = (
        "slug",
        "nombre",
        "servicio",
        "dominio",
        "activo",
        "google_tag",
        "pais",
    )


class FiltroCompra(ZvnAdmin):
    list_display = (
        "transaccion_corta",
        "fecha_corta",
        "formulario",
        "adnetwork",
        "producto",
        "cliente",
        "estado",
    )
    actions = [
        "envia_correo_pin",
        "envia_correo_compra",
        "envia_correo_compra_a_cliente",
        "asigna_cliente_compra",
        "busca_adnetwork",
        "paga_compra",
    ]
    list_filter = ["formulario", "adnetwork", "estado", "broker", "producto"]
    search_fields = ["transaccion", "correo", "telefono", "nombre"]
    list_per_page = 20

    @admin.display(description="Fecha Creacion")
    def fecha_corta(self, obj):
        return obj.creado.strftime("%Y-%m-%d @ %H:%M:%S")

    @admin.display(description="Transacción")
    def transaccion_corta(self, obj):
        dauuid = str(obj.transaccion)
        return dauuid[:13]

    @admin.action(description="Enviar Correo de Notificacion de Compra")
    def envia_correo_compra(self, request, queryset):
        try:
            crea_correos_notificacion_compra(info_compra=queryset.get())
        except Exception as e:
            self.message_user(request, "Ocurrió un error al generar los mensajes.", messages.ERROR)
            return False
        self.message_user(request, "Mensaje generado exitosamente.", messages.SUCCESS)
        return True

    @admin.action(description="Enviar correo de notificacion de compra a cliente")
    def envia_correo_compra_a_cliente(self, request, queryset):
        for compra in queryset.all():
            crea_correos_notificacion_compra_a_cliente(info_compra=compra)
            self.message_user(request, "Mensaje generado exitosamente.", messages.SUCCESS)
        return True

    @admin.action(description="Asigna Cliente a la compra")
    def asigna_cliente_compra(self, request, queryset):
        out1 = False
        out0 = False
        for compra in queryset.all():
            if not compra.cliente:
                try:
                    asigna_cliente(compra, True)
                except Exception as e:
                    logger.error(e)
                    out1 = True
                else:
                    out0 = True
            else:
                logger.warning(f"{compra=} ya tiene cliente asignado.")
                out1 = True
        if out0 and not out1:
            self.message_user(request, "Mensajes creados exitosamente.", messages.SUCCESS)
            return True
        if out0 and out1:
            self.message_user(request, "Ocurrieron algunos errores, revisar logs.", messages.WARNING)
            return True
        else:
            self.message_user(request, "No se pudo crear el mensaje, revisar logs.", messages.ERROR)
            return False

    @admin.action(description="Enviar Correo con PIN")
    def envia_correo_pin(self, request, queryset):
        sent = envia_correo_pin(info_compra=queryset.get())
        if sent:
            self.message_user(request, "Correo enviado exitosamente", messages.SUCCESS)
        else:
            self.message_user(request, "No se pudo enviar el correo", messages.ERROR)
        return True

    @admin.action(description="Buscar Adnetwork")
    def busca_adnetwork(self, request, queryset):
        for compra in queryset.all():
            adnetwork = buscar_adnetwork(info_compra=compra)
            if adnetwork:
                logger.info(f"{adnetwork=} asignado a {compra=}.")
                self.message_user(request, f"{adnetwork} asignado a {compra}.", messages.SUCCESS)
                return True
            else:
                logger.info(f"{adnetwork=} or {compra=} {compra.adnetwork=}.")
                self.message_user(request, f"No se encontró ADNetwork para {compra}", messages.WARNING)
        return True

    @admin.action(description="Ejecuta pago exitoso")
    def paga_compra(self, request, queryset):
        for compra in queryset.all():
            if compra.formulario.servicio.slug in ["fonotarot-latam"]:
                procesa_pago_firenze.apply_async(kwargs={"transaccion": compra.transaccion}, countdown=5)
            else:
                procesa_pago.apply_async(kwargs={"transaccion": compra.transaccion}, countdown=5)
            self.message_user(
                request,
                f"Procesando pago exitoso de {compra}, revisar Resultados de Celery",
                messages.SUCCESS,
            )
        return True


class FiltroMensaje(ZvnAdmin):
    list_display = ("destino", "tipo", "estado", "asunto")
    readonly_fields = (
        "fecha_envio",
        "creado",
        "modificado",
    )
    list_per_page = 20
    actions = ["reenvia_mensaje"]

    @admin.action(description="Reenviar mensajes seleccionado/s")
    def reenvia_mensaje(self, request, queryset):
        for mensaje in queryset.all():
            logger.info(f"Cambiando {mensaje.estado=} a not_sent")
            mensaje.estado = "not_sent"
            mensaje.save()
            if envia_mensaje(mensaje):
                self.message_user(request, "Mensaje enviado exitosamente", messages.SUCCESS)
            else:
                self.message_user(request, "Ocurrió un error, revisar logs.", messages.ERROR)
        return True


class FiltroAdnetworks(ZvnAdmin):
    list_display = ("nombre", "slug", "accion", "url", "activo")
    prepopulated_fields = {"slug": ("nombre",)}


class FiltroPromociones(ZvnAdmin):
    list_display = (
        "nombre",
        "activo",
        "formulario",
        "prints",
        "clicks",
    )
    readonly_fields = ("prints", "clicks", "compras", "creado", "modificado")
    prepopulated_fields = {"slug": ("nombre",)}


admin.site.register(Servicio, FiltroServicio)
admin.site.register(Cliente, FiltroCliente)
admin.site.register(Producto, FiltroProducto)
admin.site.register(Telefono, FiltroTelefono)
admin.site.register(Correo, FiltroCorreo)
admin.site.register(Tienda, FiltroTienda)
admin.site.register(Formulario, FiltroFormulario)
admin.site.register(Compra, FiltroCompra)
admin.site.register(Mensaje, FiltroMensaje)
admin.site.register(Adnetwork, FiltroAdnetworks)
admin.site.register(Promocion, FiltroPromociones)
