# Generated by Django 4.1.2 on 2022-10-20 22:37

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("zvn", "0031_adnetwork"),
    ]

    operations = [
        migrations.AlterField(
            model_name="adnetwork",
            name="accion",
            field=models.CharField(
                blank=True,
                choices=[
                    (None, "No Ejecutar Postback"),
                    ("GET", "GET"),
                    ("POST", "POST"),
                    ("PUT", "PUT"),
                    ("HEAD", "HEAD"),
                    ("OPTIONS", "OPTIONS"),
                ],
                default=None,
                max_length=10,
                null=True,
                verbose_name="Accion Postback",
            ),
        ),
        migrations.AlterField(
            model_name="adnetwork",
            name="url",
            field=models.CharField(
                blank=True, max_length=2048, null=True, verbose_name="URL Postback"
            ),
        ),
    ]
