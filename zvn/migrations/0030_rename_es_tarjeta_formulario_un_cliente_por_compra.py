# Generated by Django 4.1.2 on 2022-10-14 17:23

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("zvn", "0029_alter_compra_url"),
    ]

    operations = [
        migrations.RenameField(
            model_name="formulario",
            old_name="es_tarjeta",
            new_name="un_cliente_por_compra",
        ),
    ]
