from django.apps import AppConfig


class ZvnConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "zvn"
    is_module = True

    def ready(self):
        from zvn import signals
