import logging

from django.conf import settings
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver

from zvn.merchants import Merchant
from zvn.models import Compra, Formulario, Mensaje
from zvn.stuff import crea_htmls
from zvn.tasks import envia_mensaje, nueva_compra

logger = logging.getLogger("django")


@receiver(post_save, sender=Formulario)
def post_save_formulario(sender, instance, created, raw, using, update_fields, **kwargs):
    if created:
        # crea_htmls(formulario=instance, overwrite=True)
        pass
    return True


@receiver(pre_save, sender=Compra)
def pre_save_compra(sender, instance, raw, using, update_fields, **kwargs):
    if not instance.broker_pago_id and not update_fields:
        logger.info(f"{instance.transaccion} no tiene broker_pago_id")
        api = Merchant()
        return_url = f"{instance.formulario.servicio.url_ok}/"
        cancel_url = f"{instance.formulario.servicio.url_nok}/"
        payload = {
            "broker": instance.broker,
            "amount": instance.producto.obtiene_monto(),
            "currency": instance.producto.moneda,
            "email": instance.correo,
            "description": instance.producto.descripcion,
            "return_url": return_url,
            "cancel_url": cancel_url,
            "notification_url": f"{settings.MERCHANTS_RETURN_URL}/{instance.transaccion}",
        }
        instance.broker_payload = payload
        try:
            pago = api.crear_pago(payload=payload)
            instance.broker_response = pago
            logger.info(f"Pago {pago['transaction']} generado con {instance.broker}")
        except Exception as error:
            logger.error(f"{error=}")
            return False
        else:
            instance.broker_pago_id = pago["transaction"]
            instance.url = pago["url"]

    return True


@receiver(post_save, sender=Compra)
def post_save_compra(sender, instance, created, **kwargs):
    if created and instance.estado == "Pendiente":
        logger.info(f"nueva_compra.apply_async(kwargs='compra': {instance.transaccion}, countdown=5)")
        nueva_compra.apply_async(kwargs={"compra": instance.transaccion}, countdown=5)
    return True


@receiver(post_save, sender=Mensaje)
def post_save_mensaje(sender, instance, created, **kwargs):
    if created and instance.estado == "not_sent":
        logger.info(f"envia_mensaje(mensaje={instance})")
        return envia_mensaje(mensaje=instance)
