from marshmallow import Schema, fields


class Fonotarot(Schema):
    utm_source = fields.Str(required=True)
    utm_medium = fields.Str(required=True)
    utm_campaign = fields.Str(required=True)
    utm_term = fields.Str(required=True)
    utm_content = fields.Str(required=True)
