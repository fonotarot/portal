# Generated by Django 4.1.2 on 2022-10-20 22:12

import jsonfield.fields
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("zvn", "0030_rename_es_tarjeta_formulario_un_cliente_por_compra"),
    ]

    operations = [
        migrations.CreateModel(
            name="Adnetwork",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("nombre", models.CharField(max_length=255)),
                ("reglas", jsonfield.fields.JSONField(verbose_name="Reglas Adnetwork")),
                (
                    "accion",
                    models.CharField(
                        choices=[
                            ("GET", "GET"),
                            ("POST", "POST"),
                            ("PUT", "PUT"),
                            ("HEAD", "HEAD"),
                            ("OPTIONS", "OPTIONS"),
                        ],
                        default="GET",
                        max_length=10,
                        verbose_name="Accion Postback",
                    ),
                ),
                ("url", models.CharField(max_length=2048, verbose_name="URL Postback")),
                (
                    "params",
                    jsonfield.fields.JSONField(
                        null=True, verbose_name="Parametros Postback"
                    ),
                ),
                ("activo", models.BooleanField(default=True)),
                ("creado", models.DateTimeField(auto_now_add=True)),
                ("modificado", models.DateTimeField(auto_now=True)),
            ],
            options={
                "verbose_name_plural": "Adnetworks",
            },
        ),
    ]
